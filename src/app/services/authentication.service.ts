import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";

const URL = environment.API_URL;

@Injectable({
  providedIn: "root"
})
export class AuthenticationService {

  constructor(private httpClient : HttpClient) { }

  login(userLoginObj : any) {
    const body = new HttpParams()
    .set("username", userLoginObj.username)
    .set("password", userLoginObj.password)
    .set("grant_type", userLoginObj.grant_type);

    const httpOptions = {
      headers: new HttpHeaders({
        "Content-Type": "application/x-www-form-urlencoded"
      })
    };
    return this.httpClient.post(`http://35.200.182.243/DknkApi/login`, body, httpOptions);
  }

  logout() {
    sessionStorage.removeItem("currentUser");
  }
}
