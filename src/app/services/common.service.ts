import { Injectable } from "@angular/core";
import { HttpClient, HttpParams, HttpHeaders, HttpResponse } from "@angular/common/http";
import {environment} from "../../environments/environment";
import { RequestOptions } from 'http';
import {DatePipe} from '@angular/common';
 
const BASE_URL = environment.API_URL;

@Injectable({
  providedIn: "root"
})

export class CommonService {
  private queryString;
  constructor(private httpClient: HttpClient,
    private Datepipe:DatePipe) { }

  onGet(url) {
    return this.httpClient.get(`${BASE_URL}${url}`);
  }

  onGetById(url, Id) {
    return this.httpClient.get(`${BASE_URL}${url}`, Id);
  }

  onGetByKey(url, key, data) {
    const params = new HttpParams().set(key, data);
    return this.httpClient.get(`${BASE_URL}${url}`,{params});
  }
  
  onDownload(url,suburl, key, data) {
    const params = new HttpParams().set(key, data);
    return this.httpClient.get(`${BASE_URL}${url}/${suburl}`,{responseType: 'arraybuffer',params});
  }

  onDownloadThirdParty(url) {
    return this.httpClient.get(`${BASE_URL}${url}`,{responseType: 'arraybuffer'});
  }

  onDownloadForAll(url) {
    return this.httpClient.get(`${BASE_URL}${url}`,{responseType: 'blob'});
  }

  onCreateUpdate(url, data : any) {
    return this.httpClient.post(`${BASE_URL}${url}`, data);
  }

  onUpdate(url, data : any) {
    return this.httpClient.put(`${BASE_URL}${url}`, data);
  }

  onFileUpload(url, formData) {
    return this.httpClient.post(`${BASE_URL}${url}`, formData);
  }

  onDelete(url, key){
    return this.httpClient.delete(`${BASE_URL}${url}`, key);
  }
  
  Date(){
    return this.Datepipe.transform(new Date(),"dd-MM-yyyy");
  }
}
