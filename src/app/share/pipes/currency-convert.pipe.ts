import { Pipe, PipeTransform } from "@angular/core";
import {environment} from "../../../environments/environment"

@Pipe({
  name: "currencyConvert"
})
export class CurrencyConvertPipe implements PipeTransform {
  costDivision = environment.costDivision;
  transform(value: any, ...args: any[]): any {
    var val: any;
    val = Math.abs(value);
    val = (val / this.costDivision).toFixed(2);
    return val;
  }

}
