
import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from "@angular/router";
import { Observable } from "rxjs";
import { AuthenticationService } from "../services";

@Injectable({ providedIn: "root" })
export class AuthGuardService implements CanActivate {

    public currentUser = null;
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) { }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        this.currentUser = sessionStorage.getItem("currentUser");
        if (this.currentUser != null) {
          return true;
        } else {
          this.router.navigate(["login"]);
        }
      }
}