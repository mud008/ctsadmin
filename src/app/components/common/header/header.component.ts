import { Component, OnInit, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html"
})
export class HeaderComponent implements OnInit {
  @Output() changeClass = new EventEmitter<boolean>();
  clicked = false;
  constructor() { }

  ngOnInit() {
  }
  
  mobuileviewChanged() { 
    this.clicked = !this.clicked;
    this.changeClass.emit(this.clicked);
  }
}
