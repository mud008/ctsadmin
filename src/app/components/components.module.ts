import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ComponentsRoutingModule } from "./components-routing.module";
import { ChartsModule } from "ng2-charts";
import { OrgChartModule } from 'ng2-org-chart';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    OrgChartModule,
    ComponentsRoutingModule,
    ChartsModule
  ],

})
export class ComponentsModule { }
