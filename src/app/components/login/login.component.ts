import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../../services/authentication.service";
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = "";

  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private authService : AuthenticationService) { 
      const accesstoken = sessionStorage.getItem("currentUser");
      if (accesstoken) { 
        this.router.navigate(["/"]);
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ["Naveen", Validators.required],
      password: ["Naveen", Validators.required],
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {return;}
    this.loading = true;
    let data = {
      username : this.f.username.value,
      password : this.f.password.value,
      grant_type : "password"
    };

    this.authService.login(data).subscribe(
      user => {
        this.toastr.success("User has been login successfully!", "Success", {
          timeOut: 3000,
        });
        sessionStorage.setItem("currentUser", JSON.stringify(user));
        this.router.navigate([this.returnUrl]);
      },error => {
          this.toastr.error(error.message ? error.message : "Please try again!", "Error", {
            timeOut: 3000,
          });
          this.error = error;
          this.loading = false;
      });
    }
}
