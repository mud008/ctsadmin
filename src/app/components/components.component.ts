import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-components",
  templateUrl: "./components.component.html",
})
export class ComponentsComponent implements OnInit {

  addClass = false;
  constructor() { }

  ngOnInit() {
  }

  changeClass(add: boolean) {
    this.addClass = add ? true : false;
   }
}
