import { Component, OnInit, TemplateRef, ViewChild } from "@angular/core";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { CommonService } from "src/app/services";
import {environment} from "src/environments/environment"
import { BsModalService, BsModalRef, ModalDirective } from "ngx-bootstrap/modal";
import * as jsPDF from "jspdf";
import * as Excel from "exceljs/dist/exceljs.min.js";
import * as fs from "file-saver";
import { DataTableDirective } from 'angular-datatables';
import { DatePipe } from '@angular/common';
import { Subject } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SwalComponent } from '@sweetalert2/ngx-sweetalert2';
@Component({
  selector: 'app-infected',
  templateUrl: './infected.component.html',
  styleUrls: ['./infected.component.scss']
})
export class InfectedComponent implements OnInit {
  @ViewChild('deleteSwal', { static: true }) private deleteSwal: SwalComponent;

  public infectedPeople: any;
  infectedForm: FormGroup;
  submitted = false;
  
  public viewDashboardColumnTitle = "";
  public viewDashboardDetailRoute = "";
  modalRef : BsModalRef;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  
  public pdfFileName = `Infected-People-Details-${this.Datepipe.transform(new Date(),"dd-MM-yyyy")}.pdf`;

  constructor(
    private ngxService: NgxUiLoaderService,
    private activatedRoute: ActivatedRoute,
    private modalService: BsModalService,
    private Datepipe:DatePipe,
    private formBuilder: FormBuilder,
    private router: Router, private commonService : CommonService
  ) { }

  ngOnInit() {
    this.infectedForm = this.formBuilder.group({
      MobileNo: ['', Validators.required],
      AffectedDate: ['', Validators.required],
    });

    this.viewDashboardColumnTitle = "View Infected-People";
    this.onGetTracedetails();
  }

  
  //get all listing data
  onGetTracedetails(){

    this.ngxService.start();
    this.dtOptions = {
      pagingType: "full_numbers",
      pageLength: 12
    };
    this.commonService.onGet("Reference").subscribe(res => {
      // destroys the data table in order to reinitialize it
      $('#DataTables_Table_0').DataTable().destroy();
      this.infectedPeople = res['Data'];
      this.dtTrigger.next(); 
      this.ngxService.stop();
      },
      error => this.ngxService.stop()
    );
    
  }

  onViewDetailDashboard(type: string, value: string) {
    this.router.navigate(["departments/view"], { queryParams: { type, value } });
  }

  onOpenModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template,  {class: 'modal-lg'});
  }

  // convenience getter for easy access to form fields
  get f() { return this.infectedForm.controls; }

  onSubmit(form) {
      form.value.AffectedDate = this.Datepipe.transform(form.value.AffectedDate, "dd-MMM-yyyy")
      console.log(form.value);
      this.commonService.onCreateUpdate("Reference",form.value).subscribe(
        data => {
          console.log(data);
          form.reset();
          this.onGetTracedetails();
          this.modalClose();
        },
        (error: any) => console.log(error)
      );    
      if (this.infectedForm.invalid) {
          return;
      }
  }

  //Delete row
  deleteFile(nomber) {
    this.commonService.onCreateUpdate("Reference/Delete?key="+nomber, nomber).subscribe(
      data => {
        console.log(data);
        this.onGetTracedetails();
      },
      (error: any) => console.log(error)
    );    
  }


  // get event from child component and close the modal 
  modalClose(){
    this.modalRef.hide();
  }

  expoerExcel(){
    const title = "Infected People Details";
    const header = ["#", "Mobile Number" , "Unique Id", "Date"];
    
      let data = [];
      this.infectedPeople.forEach((element, key) => {
        let newArray = [];
        newArray.push(
        key + 1,
        element['MobileNo'] ? element['MobileNo'] : '-', 
        element['SourceBlueTooth'] ? element['SourceBlueTooth'] : '-',
        element['Time'] ? element['Time'] :'-',
        
        )
        data[key]=newArray;
      });
  
        let workbook = new Excel.Workbook();
        let worksheet = workbook.addWorksheet('Infected_People_Details');
        let titleRow = worksheet.addRow([title]);
        titleRow.getCell(1).fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'D2DCE2' }
        };
        // Set font, size and style in title row.
        worksheet.properties.defaultRowHeight = 20;
        worksheet.properties.defaultRowWidth = 500;
        titleRow.font = { name: 'Calibri', family: 4, size: 14, underline: 'double', bold: true };
        // Blank Row
        worksheet.addRow([]);
        worksheet.mergeCells('A1:D2');
        worksheet.addRow([]);
        
        worksheet.columns = [
          {width:5},
          {width:30},
          {width:30},
          {width:30},
        ];
        
        let headerRow = worksheet.addRow(header);
        headerRow.font = {bold: true };
        
        headerRow.eachCell((cell, number) => {
        cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: 'D2DCE2' },
        bgColor: { argb: 'FFFFFF00' }
        }
        cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } }
        });
        worksheet.addRows(data);
        worksheet.addRows([]);
        worksheet.addRows([]);
        //Footer Row
      
        
        worksheet.getRow(1).alignment = { horizontal: 'center',vertical : 'middle'};
       
        
        workbook.xlsx.writeBuffer().then((data) => {
        let blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        fs.saveAs(blob, `${this.commonService.Date()}_Infected_People_Details.xlsx`);
        });
  
    }

    exportPDF() {
      const doc = new jsPDF("l", "pt", "tabloid");
  
    // add custom font to file
     //doc.addFileToVFS("tunga.ttf", "Tunga");
  //   doc.addFileToVFS('tunga.ttf', 'tunga.ttf  ');
  
  
   //   doc.addFont('tunga.ttf', 'Tunga', 'normal');
   //   doc.setFont('Tunga');
   //   console.log(doc.getFontList());
  
   const header = ["Infected People", "#", "Mobile Number" , "Unique Id", "Date"];
      
      doc.autoTable({
        head: [header],
        margin: { top: 80 },
        headStyles: { europe: { halign: "center" } }, 
        html: "#basic-table",
        columnStyles:{
          0: {cellWidth: 780},
          2: {cellWidth: 30},
          3: {cellWidth: 250},
          4: {cellWidth: 200},
          5: {cellWidth: 300},
        },
        bodyStyles: { fontSize: 8,lineWidth:1, cellPadding: 5} // Font Size for Rows
      });
      console.log(doc);
      doc.save(this.pdfFileName);
    }
    
}