import { NgModule } from "@angular/core";
import { AgmCoreModule } from "@agm/core";
import { CommonModule } from "@angular/common";
import {DatePipe} from "@angular/common";
import { ContainerRoutingModule } from "./container-routing.module";
import { OrgChartModule } from 'ng2-org-chart';

import { DashboardComponent } from "./dashboard/dashboard.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { ModalModule } from "ngx-bootstrap/modal";
import { ChartsModule } from "ng2-charts";
import { MalihuScrollbarModule } from "ngx-malihu-scrollbar";
import { CarouselModule } from "ngx-owl-carousel-o";
import { AccordionModule } from "ngx-bootstrap/accordion";
import { NgCircleProgressModule } from "ng-circle-progress";
import { NgMultiSelectDropDownModule } from "ng-multiselect-dropdown";
import { MatDatepickerModule, MatNativeDateModule, MatFormFieldModule, MatInputModule,MatMenuModule,MatSelectModule} from "@angular/material";
import { DataTablesModule } from "angular-datatables";
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

// Common filter
import { CommonFilterComponent } from "./common-filter/common-filter.component";
//Components

import { TracesComponent} from "./tracespage/traces.component";
import { MatTableModule,MatIconModule, MatSnackBarModule} from "@angular/material";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import { MatPaginatorModule } from "@angular/material/paginator";
import { CurrencyConvertPipe } from "../../share/pipes/currency-convert.pipe";
import {NgxPaginationModule} from "ngx-pagination";
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { RegistrationsComponent } from './registrations/registrations.component';
import { InfectedComponent } from './infected-peoples/infected.component';
import { ViewreportComponent } from './viewreport/viewreport.component';
import { ChartviewComponent } from './viewreport/chartview/chartview.component';
import { TreeviewComponent } from './viewreport/treeview/treeview.component';
// import { RatingModule } from 'ng-starrating';
@NgModule({
  declarations: [
    DashboardComponent,
    CommonFilterComponent,
    TracesComponent,
    CurrencyConvertPipe,
    RegistrationsComponent,
    InfectedComponent,
    ViewreportComponent,
    ChartviewComponent,
    TreeviewComponent,
  ],
  imports: [
    CommonModule,
    OrgChartModule,
    FormsModule,
    MatMenuModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyClXoXjRap5nFM-6A7Gj-loN7pR_dOHyck"
    }),
    ContainerRoutingModule,
    MatCheckboxModule,
    MatSelectModule,
    DataTablesModule,
    ModalModule.forRoot(),
    AccordionModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    ChartsModule,
    MalihuScrollbarModule.forRoot(),
    CarouselModule,
    NgCircleProgressModule.forRoot({
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300
    }),
    MatDatepickerModule,MatNativeDateModule,MatFormFieldModule,MatInputModule,MatTableModule,MatProgressBarModule,
    MatIconModule,MatSnackBarModule,
    MatPaginatorModule,
    NgxPaginationModule,
    SweetAlert2Module.forRoot(),
    // RatingModule 
  ],
  providers:[
    DatePipe
  ],
  exports:[CommonFilterComponent]
})
export class ContainerModule { }

