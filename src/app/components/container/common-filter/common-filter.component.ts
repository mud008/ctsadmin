import { Component, OnInit, EventEmitter, Output, Input } from "@angular/core";
import { BsModalService, BsModalRef, ModalDirective } from "ngx-bootstrap/modal";
import { IDropdownSettings } from "ng-multiselect-dropdown";

@Component({ 
  selector: "app-common-filter",
  templateUrl: "./common-filter.component.html",
  styleUrls: ["./common-filter.component.scss"]
})
export class CommonFilterComponent implements OnInit {
  public allData: any;
  public modalRef: BsModalRef;
  public url = "";
  public title = "";
  public fileName = "ExcelSheet.xlsx";
  public pdfFileName = "PDFDoc.pdf";

  public dropdownList = [];

  @Input() allStatusType = [];
  public allStatusTypeSettings: IDropdownSettings = {};
  @Input() allStatusTypeSelectedItem = [];

  @Input() allTaluks = [];
  public allTaluksSettings: IDropdownSettings = {};
  @Input() allTaluksSelectedItems = [];

  @Input() allEngineers = [];
  public allEngineersSettings: IDropdownSettings = {};
  @Input() allEngineersSelectedItems = [];



  @Input() allDepartments = [];
  public allDepartmentsSettings: IDropdownSettings = {};
  @Input() allDepartmentsSelectedItems = [];

  @Input() allFinancialYear = [];
  public allFinancialYearSettings: IDropdownSettings = {};
  @Input() allFinancialYearSelectedItems = [];

  

  
  public searchProjectFilter = {
    Taluks: [],
    Departments: [],
    Engineers: [],
    ProjectStatus: [],
    FinancialYear:[],
  };

  public dataSource: any;
  public activeRouteParams: any;
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @Output() closeModel: EventEmitter<any> = new EventEmitter<any>();
  @Output() searchProject : EventEmitter<any> = new EventEmitter<any>();

  constructor(private modalService: BsModalService) { }

  ngOnInit() {
    
    this.allStatusTypeSettings = {
      singleSelection: false,
      idField: "ProjectStatus",
      textField: "ProjectStatus",
      enableCheckAll: true,
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.allTaluksSettings = {
      singleSelection: false,
      idField: "Taluk",
      textField: "Taluk",
      enableCheckAll: true,
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.allEngineersSettings = {
      singleSelection: false,
      idField: "Engineer",
      textField: "Engineer",
      enableCheckAll: true,
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    
    this.allDepartmentsSettings = {
      singleSelection: false,
      idField: "Department",
      textField: "Department",
      enableCheckAll: true,
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    
    this.allFinancialYearSettings = {
      singleSelection: false,
      idField: "FinancialYear",
      textField: "FinancialYear",
      enableCheckAll: true,
      selectAllText: "Select All",
      unSelectAllText: "UnSelect All",
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    
    
    
  }

  onCloseModal(){ 
    this.closeModel.emit();
  }

  onApplyProjectFilter() {
    this.allData = [];
    this.searchProjectFilter.Taluks = [];
    this.searchProjectFilter.Departments = [];
    this.searchProjectFilter.Engineers = [];
    this.searchProjectFilter.ProjectStatus = [];
    this.searchProjectFilter.FinancialYear = [];
    
    for (const taluk of this.allTaluksSelectedItems) {
      this.searchProjectFilter.Taluks.push(taluk);
    }
    for (const department of this.allDepartmentsSelectedItems) {
      this.searchProjectFilter.Departments.push(department);
    }

    for (const engineers of this.allEngineersSelectedItems) {
      this.searchProjectFilter.Engineers.push(engineers);
    }
    for (const status of this.allStatusTypeSelectedItem) {
      this.searchProjectFilter.ProjectStatus.push(status);
    }

    //Financial Year
    for (const financial_year of this.allFinancialYearSelectedItems) {
      this.searchProjectFilter.FinancialYear.push(financial_year);
    }



    this.searchProject.emit(this.searchProjectFilter);
  }

  onItemSelect(item: any, from: string) {
    switch (from) {
      case "taluks":
        if (this.searchProjectFilter.Taluks.length === 0) {
          this.searchProjectFilter.Taluks.push(item);
        } else {
          for (const taluk of this.searchProjectFilter.Taluks) {
            if (taluk !== item) {
              this.searchProjectFilter.Taluks.push(item);
              return;
            }
          }
        }
        break;
      case "departments":
        if (this.searchProjectFilter.Departments.length === 0) {
          this.searchProjectFilter.Departments.push(item);
        } else {
          for (const dept of this.searchProjectFilter.Departments) {
            if (dept !== item) {
              this.searchProjectFilter.Departments.push(item);
              return;
            }
          }
        }
        break;
      case "financial_year":
        if (this.searchProjectFilter.FinancialYear.length === 0) {
          this.searchProjectFilter.FinancialYear.push(item);
        } else {
          for (const fyear of this.searchProjectFilter.FinancialYear) {
            if (fyear !== item) {
              this.searchProjectFilter.FinancialYear.push(item);
              return;
            }
          }
        }
        break;
      case "Engineers":
        if (this.searchProjectFilter.Engineers.length === 0) {
          this.searchProjectFilter.Engineers.push(item);
        } else {
          for (const eng of this.searchProjectFilter.Engineers) {
            if (eng !== item) {
              this.searchProjectFilter.Engineers.push(item);
              return;
            }
          }
        }
        break;
      case "status":
        if (this.searchProjectFilter.ProjectStatus.length === 0) {
          this.searchProjectFilter.ProjectStatus.push(item.ProjectStatus);
        } else {
          for (const status of this.searchProjectFilter.ProjectStatus) {
            if (status !== item) {
              this.searchProjectFilter.ProjectStatus.push(item);
              return;
            }
          }
        }
        break;
      default:
        break;
    }
  }

  onItemDeSelect(item: any, from: string) {
    switch (from) {
      case "taluks":
        for (const Taluk of this.searchProjectFilter.Taluks) {
          if (Taluk === item) {
            this.searchProjectFilter.Taluks.splice(this.searchProjectFilter.Taluks.indexOf(item), 1);
            return;
          }
        }
        break;
      case "departments":
        for (const dept of this.searchProjectFilter.Departments) {
          if (dept === item) {
            this.searchProjectFilter.Departments.splice(this.searchProjectFilter.Departments.indexOf(item), 1);
            return;
          }
        }
        break;
      
      case "engineers":
        for (const eng of this.searchProjectFilter.Engineers) {
          if (eng === item) {
            this.searchProjectFilter.Engineers.splice(this.searchProjectFilter.Engineers.indexOf(item), 1);
            return;
          }
        }
        break;
      case "status":
        for (const status of this.searchProjectFilter.ProjectStatus) {
          if (status === item) {
            this.searchProjectFilter.ProjectStatus.splice(this.searchProjectFilter.ProjectStatus.indexOf(item), 1);
            return;
          }
        }
        break;
      case "financialyear":
        for (const fin of this.searchProjectFilter.FinancialYear) {
          if (fin === item) {
            this.searchProjectFilter.FinancialYear.splice(this.searchProjectFilter.FinancialYear.indexOf(item), 1);
            return;
          }
        }
        break;
      default:
        break;
    }
  }
}
