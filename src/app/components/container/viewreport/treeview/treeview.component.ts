import { Component, OnInit } from '@angular/core';
import { IEmployee } from 'ng2-org-chart';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from 'src/app/services';
import { IContact } from '../../IContact';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-treeview',
  templateUrl: './treeview.component.html',
  styleUrls: ['./treeview.component.scss']
})
export class TreeviewComponent implements OnInit {
  contactChart: IContact;
  infectedPeoples: any;
  moileNumber: number;
  firstData: any;
  expandeds: any = {};
  listData: any[]=[];
  public index = 0;
  constructor(private formBuilder: FormBuilder, private router: Router, private commonService : CommonService,     private activatedRoute: ActivatedRoute) { }
  
  ngOnInit() {
    this.activatedRoute.params.subscribe( params => this.moileNumber =  params.MobileNo );

    //infected people
    this.commonService.onGet("Reference").subscribe(res =>{
        
        this.infectedPeoples = res['Data'];
        console.log(this.infectedPeoples[0].MobileNo);
        this.moileNumber = this.moileNumber? this.moileNumber : this.infectedPeoples[0].MobileNo;
        ///get chart Tree data for first number

        this.getChartData(this.moileNumber);

    });
     
  }

 
//on chage in select box
  onChangeNumber(MobileNo){
        ///get chart Tree data for selected number

    this.getChartData(MobileNo);
  }

    ///get chart Tree data 
  getChartData(MobileNo){
    this.moileNumber = MobileNo;
    // Get Chart data
    this.commonService.onGet("Tree?MobileNo=" + MobileNo).subscribe(res =>{
        
        this.contactChart = res['Data'];
        console.log(this.contactChart);

        this.expandeds[MobileNo] = {number: MobileNo, open: false};
    });
    this.router.navigate(['/tracereport/tree/'+MobileNo]);
  }

  
  onExpanded(mobNumber){
    // this.expanded.open = (this.expanded.key == mobNumber) ? !this.expanded.open : true;
   //  this.expanded.key = (this.expanded.key== mobNumber) ? '' : mobNumber;
     
     if(!this.expandeds[mobNumber])
       this.expandeds[mobNumber] = {number: mobNumber, open: false};
 
     this.expandeds[mobNumber].open = (this.expandeds[mobNumber].number == mobNumber) ? !this.expandeds[mobNumber].open :  true;
         
     console.log(this.expandeds);
   }
}
