import { Component, OnInit, TemplateRef } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NgxUiLoaderService } from "ngx-ui-loader";
import { CommonService } from "../../../services/index";
import { Label } from "ng2-charts";
import { ChartOptions, ChartType, ChartDataSets } from "chart.js";
import * as pluginDataLabels from "chartjs-plugin-datalabels";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { element } from "protractor";
import html2canvas from "html2canvas";
import * as jsPDF from "jspdf";
import * as Excel from "exceljs/dist/exceljs.min.js";
import * as fs from "file-saver";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {
  public costDivision = environment.costDivision;
  costTitlte = environment.costTitlte;
  public oneAtATime = true;
  public dtOptions: DataTables.Settings = {

      pagingType: "full_numbers",
      pageLength: 10,
      scrollX: true,
      lengthChange: false,
      scrollY: "400px",
      responsive: true,
      
  
  };
  
  public displayTitle;
  public scrollbarOptions = { axis: "y", theme: "minimal-dark" };
  public dashboardData: any;
  public agencyProgress: any;
  public agencyData: any[] = [];
  public lat: string;
  public lng: string;
  public modalRef: BsModalRef;
  public individualProjectDetails: any;
  public dashboardSummary: any;
  public dashboardDetailData: any;

  public finance=[];
  public Delaye=[];
  public ProjectStatuss=[];
  public ProgressData=[]

  public projectStatusChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    // We use these empty structures as placeholders for dynamic theming.
    scales: {
      xAxes: [{ ticks: { mirror: true } }],
      yAxes: [{ ticks: { mirror: true } }]
    },
    plugins: {
      datalabels: {
        anchor: "end",
        align: "end"
      }
    }
  };
  public projectStatusChartLabels: Label[] = [];
  public projectStatusChartType: ChartType = "bar";
  public projectStatusChartLegend = false;
  public projectStatusChartPlugins = [pluginDataLabels];
  public projectStatusChartDataProjectCount = [];
  public projectStatusChartData: ChartDataSets[] = [
    { data: this.projectStatusChartDataProjectCount, label: "STATUS" }
  ];

  public projectProgressChartLabels: Label[] = [];
  public projectProgressChartDataProjectCount = [];
  public projectProgressChartData: ChartDataSets[] = [
    { data: this.projectProgressChartDataProjectCount, label: "PROGRESS" }
  ];

  public financialReportChartLabels: Label[] = [];
  public financialReportChartDataSanctionedCost = [];
  public financialReportChartDataReceived = [];
  public financialReportChartDataExpenses = [];
  public financialReportChartData: ChartDataSets[] = [
    { data: this.financialReportChartDataSanctionedCost, label: "SANCTIONED" },
    { data: this.financialReportChartDataReceived, label: "Received" },
    { data: this.financialReportChartDataExpenses, label: "EXPENSES" }
  ];

  public projectDelayedChartLabels: Label[] = [];
  public projectDelayedChartDataProjectCount = [];
  public projectDelayedChartData: ChartDataSets[] = [
    { data: this.projectDelayedChartDataProjectCount, label: "Series A" }
  ];

  Graphdata = {
    ProjectStatus: false,
    ProgressReport: false,
    FinancialReport: true,
    DelayedProjects: false
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private ngxService: NgxUiLoaderService,
    private commonService: CommonService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    //this.onGetDashboardData();
  }

  

  onGoogleMapIconClick(
    template: TemplateRef<any>,
    individualProjectDetails: object
  ) {
    this.individualProjectDetails = individualProjectDetails;
    this.modalRef = this.modalService.show(
      template,
      Object.assign({}, { class: "modal-sm location-modal-wrapper" })
    );
  }

  

  ExportModel(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(
      template,
      Object.assign(
        {},
        { class: "modal-lg center global-create-modal-wrapper" }
      )
    );
    this.dtOptions = {
      // lengthChange: false
      pagingType: "full_numbers",
      pageLength: 10,
      scrollX: true,
      lengthChange: false,
      scrollY: "400px",
      responsive: true
    };
  }

  chartClicked(e: any, template: TemplateRef<any>, type: string) {
    this.ngxService.start();

    let URL = "";

    if (e.active.length > 0) {
      const chart = e.active[0]._chart;
      const activePoints = chart.getElementAtEvent(e.event);
      if (activePoints.length > 0) {
        // get the internal index of slice in pie chart
        const clickedElementIndex = activePoints[0]._index;
        const label = chart.data.labels[clickedElementIndex];
        if (type == "Financial_Report") {
          this.displayTitle = "Financial Report";
          URL = `FinancialYear/ProjectLists?FinancialYear=${label}`;
        }

        if (type == "Delayed_Projects") {
          URL = `DelayedBucket/ProjectLists?DelayedBucket=${label}`;
          this.displayTitle = "Delayed Projects";
        }

        if (type == "Project_Status_Report") {
          this.displayTitle = "Project Status Report";
          URL = `ProjectStatus/ProjectLists?ProjectStatus=${label}`;
        }

        if (type == "Progress_Report") {
          this.displayTitle = "Progress Report";
          URL = `ProgressBucket/ProjectLists?ProgressBucket=${label}`;
        }

       // this.commonService.onGet(URL).subscribe(    );
      }
    }
  }

  downloadCanvas(types) {
    window.scrollTo(0, 0);
    html2canvas(document.getElementById(types)).then(function(canvas) {
      var img = canvas.toDataURL("image/png");

      fs.saveAs(img, `${types}.png`);
    });
  }

  onAgencyProgressClick(
    template: TemplateRef<any>,
    value: any,
    type: any,
    progressType
  ) {
    this.ngxService.start();
    this.dtOptions = {
      pagingType: "full_numbers",
      pageLength: 10,
      scrollX: true,
      lengthChange: false,
      scrollY: "400px",
      responsive: true
    };
    let URL = "";
    if (progressType == "AGENCY_PROGRESS") {
      this.displayTitle = "Agency Progress";
      URL = `api/Dashboard/GetDeptList?DeptName=${value}&Type=${type}`;
    }
    if (progressType == "DAILY_PROGRESS") {
      this.displayTitle = "Daily Progress";
      URL = `api/Dashboard/GetprogressList?DateBucket=${value}&Type=${type}`;
    }
    // this.commonService.commonGet(URL).subscribe(
    //   res => {
    // this.agencyProgress = res["Data"];

    this.ExportModel(template);
    this.ngxService.stop();
    // },
    // error => {
    //   this.ngxService.stop();
    // }
    // );
  }






  showAmount(event, type, item) {
    this.Graphdata[type] = !item;
    // ProjectStatus
    if (type == "ProjectStatus") {
      this.projectStatusChartDataProjectCount = [];
      this.dashboardData.ProjectStatus.forEach(element => {
        if (
          element.DisplayStatus == "PROGRESS" ||
          element.DisplayStatus == "COMPLETED"
        ) {
          if (this.Graphdata[type]) {
            this.projectStatusChartDataProjectCount.push(
              (element.Sanctioned / this.costDivision).toFixed(2)
            );
          } else {
            this.projectStatusChartDataProjectCount.push(element.Count);
          }
        }
      });
      if (this.Graphdata[type]) {
        this.projectStatusChartDataProjectCount.push(
          (
            this.dashboardData.Sanctioned / this.costDivision
          ).toFixed(2)
        );
      } else {
        this.projectStatusChartDataProjectCount.push(
          this.dashboardData.Sanctioned
        );
      }
      this.projectStatusChartData = [
        { data: this.projectStatusChartDataProjectCount, label: "Series A" }
      ];
    }

    // ProgressReport
    if (type == "ProgressReport") {
      this.projectProgressChartDataProjectCount = [];
      this.dashboardData.ProgressData.forEach(element => {
        if (this.Graphdata[type]) {
          this.projectProgressChartDataProjectCount.push(
            (element.Sanctioned / this.costDivision).toFixed(2)
          );
        } else {
          this.projectProgressChartDataProjectCount.push(element.Count);
        }
      });

      this.projectProgressChartData = [
        { data: this.projectProgressChartDataProjectCount, label: "Series A" }
      ];
    }

    // FinancialReport
    if (type == "FinancialReport") {
      this.financialReportChartDataSanctionedCost = [];
      this.financialReportChartData = [];
      this.financialReportChartDataReceived = [];
      this.financialReportChartDataExpenses = [];
      this.dashboardData.YearReport.forEach(element => {
        if (this.Graphdata[type]) {
          this.financialReportChartDataSanctionedCost.push(
            (element.Sanctioned / this.costDivision).toFixed(2)
          );
          this.financialReportChartDataReceived.push(
            (element.Received / this.costDivision).toFixed(2)
          );
          this.financialReportChartDataExpenses.push(
            (element.Expenses / this.costDivision).toFixed(2)
          );
        } else {
          this.financialReportChartDataSanctionedCost.push(
            element.Count
          );
        }
      });

      if (this.Graphdata[type]) {
        this.financialReportChartData = [
          {
            data: this.financialReportChartDataSanctionedCost,
            label: "SANCTIONED"
          },
          { data: this.financialReportChartDataReceived, label: "Received" },
          { data: this.financialReportChartDataExpenses, label: "EXPENSES" }
        ];
      } else {
        this.financialReportChartData = [
          {
            data: this.financialReportChartDataSanctionedCost,
            label: "NO PROJECTS"
          }
        ];
      }
    }

    // DelayedProjects
    if (type == "DelayedProjects") {
      this.projectDelayedChartDataProjectCount = [];
      this.dashboardData.DelayedData.forEach(element => {
        if (this.Graphdata[type]) {
          this.projectDelayedChartDataProjectCount.push(
            (element.Sanctioned / this.costDivision).toFixed(2)
          );
        } else {
          this.projectDelayedChartDataProjectCount.push(element.Count);
        }
      });
      this.projectDelayedChartData = [
        { data: this.projectDelayedChartDataProjectCount, label: "DELAYED" }
      ];
    }
  }

  convert(value: any, ...args: any[]): any {
    var val: any;
    val = Math.abs(value);
    val = (val / environment.costDivision).toFixed(2);
    return val;
  }


      //-----------Financial Project----------
      financials(type) {
        let Cost = 0;
    
        let Received = 0;
    
        let Spent = 0;
    
        
    
        const title = "Projects Details Report";
    
        const header = [
          "SL NO",
    
          "Project ID",
    
          "Name",

          "Year",

          "Category",
    
          "Cost (In Lakhs)",
    
          "Received (In Lakhs)",
    
          "Spent (In Lakhs)",

          "Status"
        ];
    
        let data = [];
    
        this.agencyProgress.forEach((element, key) => {
          let newArray = [];
    
          newArray.push(
            key + 1,
    
            element.ProjectUniqueId ? element.ProjectUniqueId : 0,
            
            element.ProjectName ? element.ProjectName : '-',
    
            element.FinancialYear ? element.FinancialYear : 0,

            element.Department ? element.Department : '-',

   
    
            element.BudgetAllocated
              ? parseFloat(this.convert(element.BudgetAllocated))
              : 0,
    
            element.Received ? parseFloat(this.convert(element.Received)) : 0,
    
            element.Expenses ? parseFloat(this.convert(element.Expenses)) : 0,

            element.ProjectStatus ? element.ProjectStatus : '-'

          );
    
          data[key] = newArray;
    
          Cost += element.BudgetAllocated
            ? parseFloat(this.convert(element.BudgetAllocated))
            : 0;
    
            Received += element.Received
            ? parseFloat(this.convert(element.Received))
            : 0;
    
            Spent += element.Expenses
            ? parseFloat(this.convert(element.Expenses))
            : 0;
    
      
        });
    
        const footer = [
          "",
    
          "",
    
         "",
         "",
         "",
    
          parseFloat(Cost.toFixed(2)),
    
          parseFloat(Received.toFixed(2)),
    
          parseFloat(Spent.toFixed(2)),
          ""
        ];
        console.log(data);
        if (type == "Excel") {
          console.log("title", title);
          this.ExportInExcelFinance(title, header, data, footer);
        }
        if (type == "PDF") {
          this.ExportInPdfFinance(title, header, data, footer);
        }
      }
    
      //-----------Financial Project PDF----------
      ExportInPdfFinance(title, header, data, footer) {
        const doc = new jsPDF("l", "pt", "tabloid");
        doc.setFontType("underline");
        doc.text(512, 60, title);
    
        doc.setProperties({
          title: `Project Details Export`
        });
        data[data.length] = footer;
        doc.autoTable({
          head: [header],
          margin: { top: 80 },
          headStyles: { europe: { halign: "center" } },
          body: data,
          columnStyles: {
            columnStyles: {
              0: { cellWidth: 40 },
              1: { cellWidth: 120 },
              2: { cellWidth: 220 },
              3: { cellWidth: 100 },
              4: { cellWidth: 100 },
              5: { cellWidth: 100 },
              6: { cellWidth: 50 },
              7: { cellWidth: 50 },
              8: { cellWidth: 50 },
              9: { cellWidth: 100 },

            }
          },
          bodyStyles: { fontSize: 7, lineWidth: 1, cellPadding: 5 }
        });
        doc.save(`${this.commonService.Date()}_Finance.pdf`);
      }

    //-------------------FinanceProject Finance Excel------------------------------
    ExportInExcelFinance(title, header, data, footer) {
      let workbook = new Excel.Workbook();
  
      let worksheet = workbook.addWorksheet("ProjectDetails");
  
      let titleRow = worksheet.addRow([title]);
  
      titleRow.getCell(1).fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "0c0b38" }
      };
      // Set font, size and style in title row.
      worksheet.properties.defaultRowHeight = 20;
      worksheet.properties.defaultRowWidth = 500;
      titleRow.font = {
        name: "Calibri",
        family: 4,
        size: 14,
        underline: "double",
        bold: true
      };
  
      // Blank Row
  
      worksheet.addRow([]);
  
      worksheet.mergeCells("A1:I1");
  
      worksheet.addRow([]);
  
      worksheet.columns = [
        { width: 5 },
  
        { width: 18 },
  
        { width: 50 },

        { width: 12 },

        { width: 15 },

        { width: 10 },
  
        { width: 10 },
  
        { width: 10 },
  
        { width: 15 }
      ];
  
      let headerRow = worksheet.addRow(header);
  
      headerRow.font = { bold: true };
  
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
  
          pattern: "solid",
  
          fgColor: { argb: "0c0b38" },
  
          bgColor: { argb: "FFFFFF00" }
        };
  
        cell.border = {
          top: { style: "thin" },
  
          left: { style: "thin" },
  
          bottom: { style: "thin" },
  
          right: { style: "thin" }
        };
      });
  
      worksheet.addRows(data);
  
      worksheet.addRows([]);
  
      worksheet.addRows([]);
  
      //Footer Row
      worksheet.getRow(1).alignment = {
        horizontal: "center",
        vertical: "middle"
      };
      let footerRow = worksheet.addRow(footer);
  
      footerRow.font = { bold: true };
  
      footerRow.getCell(6).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      footerRow.getCell(7).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      footerRow.getCell(8).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      footerRow.getCell(6).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      workbook.xlsx.writeBuffer().then(data => {
        let blob = new Blob([data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });
  
        fs.saveAs(blob, `${this.commonService.Date()}_FinancialReport.xlsx`);
      });
    }
  
    //-------------------Delayed Project REoprt------------------------------
  
    Delayeddata(type) {
      let BudgetAllocated = 0;
      
  
      const title = "Projects Details Report";
  
      const header = [
        "#",
        
        "Project Name",

        "Days",
  
        "Bucket",

        "Cost (In Lakhs)"
      ];
  
      let data = [];
  
      this.agencyProgress.forEach((element, key) => {
        let newArray = [];
  
        newArray.push(
          key + 1,

          element.ProjectName ? element.ProjectName : 0,
  
          element.DelayedDays ? element.DelayedDays : "-",
          
          element.DelayedBucket ? element.DelayedBucket : "-",
          
          
          element.BudgetAllocated
            ? parseFloat(this.convert(element.BudgetAllocated))
            : 0
        );
  
        data[key] = newArray;
  
        BudgetAllocated += element.BudgetAllocated
          ? parseFloat(this.convert(element.BudgetAllocated))
          : 0;
  
      });
  
      const footer = ["", "", "", "",parseFloat(BudgetAllocated.toFixed(2))];
  
      if (type == "Excel") {
        console.log("title", title);
        this.ExportInExcelDelayed(title, header, data, footer);
      }
      if (type == "PDF") {
        this.ExportInPdfDelayed(title, header, data, footer);
      }
    }
    //----------------Delayed Excel----------------------
    ExportInExcelDelayed(title, header, data, footer) {
      let workbook = new Excel.Workbook();
  
      let worksheet = workbook.addWorksheet("ProjectDetails");
  
      let titleRow = worksheet.addRow([title]);
  
      titleRow.getCell(1).fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "0c0b38" }
      };
      // Set font, size and style in title row.
      worksheet.properties.defaultRowHeight = 20;
      worksheet.properties.defaultRowWidth = 500;
      titleRow.font = {
        name: "Calibri",
        family: 4,
        size: 14,
        underline: "double",
        bold: true
      };
  
      // Blank Row
  
      worksheet.addRow([]);
  
      worksheet.mergeCells("A1:E1");
  
      worksheet.addRow([]);
  
      worksheet.columns = [
        { width: 5 },
  
        { width: 50 },
  
        { width: 10 },
  
        { width: 15 },
  
        { width: 10 }
      ];
  
      let headerRow = worksheet.addRow(header);
  
      headerRow.font = { bold: true };
  
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
  
          pattern: "solid",
  
          fgColor: { argb: "0c0b38" },
  
          bgColor: { argb: "FFFFFF00" }
        };
  
        cell.border = {
          top: { style: "thin" },
  
          left: { style: "thin" },
  
          bottom: { style: "thin" },
  
          right: { style: "thin" }
        };
      });
  
      worksheet.addRows(data);
  
      worksheet.addRows([]);
  
      worksheet.addRows([]);
  
      //Footer Row
      worksheet.getRow(1).alignment = {
        horizontal: "center",
        vertical: "middle"
      };
      let footerRow = worksheet.addRow(footer);
  
      footerRow.font = { bold: true };
  
  
  
      footerRow.getCell(5).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      workbook.xlsx.writeBuffer().then(data => {
        let blob = new Blob([data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });
  
        fs.saveAs(blob, `${this.commonService.Date()}_DelayedProjectReport.xlsx`);
      });
    }
    //----------delayed PDF------------
    ExportInPdfDelayed(title, header, data, footer) {
      const doc = new jsPDF("l", "pt", "tabloid");
      doc.setFontType("underline");
      doc.text(512, 60, title);
  
      doc.setProperties({
        title: `Project Details Export`
      });
      data[data.length] = footer;
      doc.autoTable({
        head: [header],
        margin: { top: 80 },
        headStyles: { europe: { halign: "center" } },
        body: data,
        columnStyles: {
          columnStyles: {
            0: { cellWidth: 40 },
            1: { cellWidth: 150 },
            2: { cellWidth: 100 },
            3: { cellWidth: 100 },
            4: { cellWidth: 120 },
            5: { cellWidth: 80 }
          }
        },
        bodyStyles: { fontSize: 7, lineWidth: 1, cellPadding: 5 }
      });
      doc.save(`${this.commonService.Date()}_Delayed.pdf`);
    }
  
    //-------------------Prgress Status------------------------------
    projectStatus(type) {
      let BudgetAllocated = 0;
      
  
      const title = "Projects Details Report";
  
      const header = [
        "#",

        "Project Name",
  
        "Display Status",

        "Cost (In Lakhs)"
      ];
  
      let data = [];
  
      this.agencyProgress.forEach((element, key) => {
        let newArray = [];
  
        newArray.push(
          key + 1,
  
          
          element.ProjectName ? element.ProjectName : "-",

          element.ProjectStatus ? element.ProjectStatus : "-",
  
          element.BudgetAllocated
            ? parseFloat(this.convert(element.BudgetAllocated))
            : 0
        );
  
        data[key] = newArray;
  
        BudgetAllocated += element.BudgetAllocated
          ? parseFloat(this.convert(element.BudgetAllocated))
          : 0;
  
   
      });
  
      const footer = ["", "", "",parseFloat(BudgetAllocated.toFixed(2))];
  
  
      if (type == "Excel") {
        console.log("title", title);
        this.ExportInExcelProjectStatus(title, header, data, footer);
      }
      if (type == "PDF") {
        this.ExportInPdfProjectStatus(title, header, data, footer);
      }
  
  
    }
    ExportInExcelProjectStatus(title, header, data, footer) {
      let workbook = new Excel.Workbook();
  
      let worksheet = workbook.addWorksheet("ProjectDetails");
  
      let titleRow = worksheet.addRow([title]);
  
      titleRow.getCell(1).fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "0c0b38" }
      };
      // Set font, size and style in title row.
      worksheet.properties.defaultRowHeight = 20;
      worksheet.properties.defaultRowWidth = 500;
      titleRow.font = {
        name: "Calibri",
        family: 4,
        size: 14,
        underline: "double",
        bold: true
      };
  
      // Blank Row
  
      worksheet.addRow([]);
  
      worksheet.mergeCells("A1:D1");
  
      worksheet.addRow([]);
  
      worksheet.columns = [
        { width: 5 },
  
        { width: 50 },
  
        { width: 20 },
  
        { width: 10 }
      ];
  
      let headerRow = worksheet.addRow(header);
  
      headerRow.font = { bold: true };
  
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
  
          pattern: "solid",
  
          fgColor: { argb: "0c0b38" },
  
          bgColor: { argb: "FFFFFF00" }
        };
  
        cell.border = {
          top: { style: "thin" },
  
          left: { style: "thin" },
  
          bottom: { style: "thin" },
  
          right: { style: "thin" }
        };
      });
  
      worksheet.addRows(data);
  
      worksheet.addRows([]);
  
      worksheet.addRows([]);
  
      //Footer Row
      worksheet.getRow(1).alignment = {
        horizontal: "center",
        vertical: "middle"
      };
      let footerRow = worksheet.addRow(footer);
  
      footerRow.font = { bold: true };
  

  
      footerRow.getCell(4).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      workbook.xlsx.writeBuffer().then(data => {
        let blob = new Blob([data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });
  
        fs.saveAs(blob, `${this.commonService.Date()}_ProgressStatusReport.xlsx`);
      });
    }
  
    ExportInPdfProjectStatus(title, header, data, footer){
      const doc = new jsPDF("l", "pt", "tabloid");
      doc.setFontType("underline");
      doc.text(512, 60, title);
  
      doc.setProperties({
        title: `Project Details Export`
      });
      data[data.length] = footer;
      doc.autoTable({
        head: [header],
        margin: { top: 80 },
        headStyles: { europe: { halign: "center" } },
        body: data,
        columnStyles: {
          columnStyles: {
            0: { cellWidth: 40 },
            1: { cellWidth: 150 },
            2: { cellWidth: 100},
            3: { cellWidth: 100 }
   
          }
        },
        bodyStyles: { fontSize: 7, lineWidth: 1, cellPadding: 5 }
      });
      doc.save(`${this.commonService.Date()}_ProjectStatus.pdf`);
  
    }
  
    //-------------------Prgress REport------------------------------
  
      progress(type){
        let BudgetAllocated = 0;
        
    
        const title = "Projects Details Report";
    
        const header = ["#", "Name","Bucket", "Sactioned Cost (In Lakhs)"];
    
        let data = [];
    
        this.agencyProgress.forEach((element, key) => {
          let newArray = [];
    
          newArray.push(
            key + 1,
            element.ProjectName ? element.ProjectName : 0,
    
            element.ProgressBucket ? element.ProgressBucket : "-",
    
    
            element.BudgetAllocated
              ? parseFloat(this.convert(element.BudgetAllocated))
              : 0
          );
    
          data[key] = newArray;
    
          BudgetAllocated  += element.BudgetAllocated
            ? parseFloat(this.convert(element.BudgetAllocated))
            : 0;
    
        
        });
    
        const footer = ["", "", "", parseFloat(BudgetAllocated.toFixed(2))];
  
        if (type == "Excel") {
          console.log("title", title);
          this.ExportInExcelProgress(title, header, data, footer);
        }
        if (type == "PDF") {
          this.ExportInPdfProgress(title, header, data, footer);
        }
    
        
      }
  
    ExportInExcelProgress(title, header, data, footer) {
  
  
      let workbook = new Excel.Workbook();
  
      let worksheet = workbook.addWorksheet("ProjectDetails");
  
      let titleRow = worksheet.addRow([title]);
  
      titleRow.getCell(1).fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "0c0b38" }
      };
      // Set font, size and style in title row.
      worksheet.properties.defaultRowHeight = 20;
      worksheet.properties.defaultRowWidth = 500;
      titleRow.font = {
        name: "Calibri",
        family: 4,
        size: 14,
        underline: "double",
        bold: true
      };
  
      // Blank Row
  
      worksheet.addRow([]);
  
      worksheet.mergeCells("A1:D1");
  
      worksheet.addRow([]);
  
      worksheet.columns = [
        { width: 5 },
  
        { width: 50 },
  
        { width: 13 },
  
        { width: 10 }
      ];
  
      let headerRow = worksheet.addRow(header);
  
      headerRow.font = { bold: true };
  
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
  
          pattern: "solid",
  
          fgColor: { argb: "0c0b38" },
  
          bgColor: { argb: "FFFFFF00" }
        };
  
        cell.border = {
          top: { style: "thin" },
  
          left: { style: "thin" },
  
          bottom: { style: "thin" },
  
          right: { style: "thin" }
        };
      });
  
      worksheet.addRows(data);
  
      worksheet.addRows([]);
  
      worksheet.addRows([]);
  
      //Footer Row
      worksheet.getRow(1).alignment = {
        horizontal: "center",
        vertical: "middle"
      };
      let footerRow = worksheet.addRow(footer);
  
      footerRow.font = { bold: true };

  
      footerRow.getCell(4).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      workbook.xlsx.writeBuffer().then(data => {
        let blob = new Blob([data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });
  
        fs.saveAs(blob, `${this.commonService.Date()}_ProjectPregressReport.xlsx`);
      });
    }
  
    ExportInPdfProgress(title, header, data, footer){
  
      const doc = new jsPDF("l", "pt", "tabloid");
      doc.setFontType("underline");
      doc.text(512, 60, title);
  
      doc.setProperties({
        title: `Project Details Export`
      });
      data[data.length] = footer;
      doc.autoTable({
        head: [header],
        margin: { top: 80 },
        headStyles: { europe: { halign: "center" } },
        body: data,
        columnStyles: {
          columnStyles: {
            0: { cellWidth: 40 },
            1: { cellWidth: 150 },
            2: { cellWidth: 120 },
            3: { cellWidth: 100 }
   
          }
        },
        bodyStyles: { fontSize: 7, lineWidth: 1, cellPadding: 5 }
      });
      doc.save(`${this.commonService.Date()}_ProjectProgress.pdf`);
  
    }





       //-----------Financial Project----------
  financialsexport(type) {
    let Sanctioned = 0;

    let Received = 0;

    let Expenses = 0;

    let NoOfProjects = 0;

    const title = "Projects Details Report";

    const header = [
      "#",

      "Year",

      "No Of Projects",

      "Sanctioned Cost (In Lakhs)",

      "RECIEVED Cost (In Lakhs)",

      "Expenses (In Lakhs)"
    ];

    let data = [];

    this.finance.forEach((element, key) => {
      let newArray = [];

      newArray.push(
        key + 1,

        element.Year ? element.Year : 0,

        element.TotalProjects ? parseFloat(element.TotalProjects) : 0,

        element.BUDGETALLOCATED
          ? parseFloat(this.convert(element.BUDGETALLOCATED))
          : 0,

        element.RECIEVED ? parseFloat(this.convert(element.RECIEVED)) : 0,

        element.EXPENSES ? parseFloat(this.convert(element.EXPENSES)) : 0
      );

      data[key] = newArray;

      Sanctioned += element.BUDGETALLOCATED
        ? parseFloat(this.convert(element.BUDGETALLOCATED))
        : 0;

      Received += element.RECIEVED
        ? parseFloat(this.convert(element.RECIEVED))
        : 0;

      Expenses += element.EXPENSES
        ? parseFloat(this.convert(element.EXPENSES))
        : 0;

      NoOfProjects += element.TotalProjects
        ? parseFloat(element.TotalProjects)
        : 0;
    });

    const footer = [
      "",

      "",

      NoOfProjects,

      parseFloat(Sanctioned.toFixed(2)),

      parseFloat(Received.toFixed(2)),

      parseFloat(Expenses.toFixed(2)),
    ];
    console.log(data);
    if (type == "Excel") {
      console.log("title", title);
      this.ExcelFinances(title, header, data, footer);
    }
    if (type == "PDF") {
      this.PdfFinances(title, header, data, footer);
    }
  }


    //-----------Financial Project PDF----------
    PdfFinances(title, header, data, footer) {
      const doc = new jsPDF("l", "pt", "tabloid");
      doc.setFontType("underline");
      doc.text(512, 60, title);
  
      doc.setProperties({
        title: `Project Details Export`
      });
      data[data.length] = footer;
      doc.autoTable({
        head: [header],
        margin: { top: 80 },
        headStyles: { europe: { halign: "center" } },
        body: data,
        columnStyles: {
          columnStyles: {
            0: { cellWidth: 40 },
            1: { cellWidth: 100 },
            2: { cellWidth: 100 },
            3: { cellWidth: 100 },
            4: { cellWidth: 100 },
            5: { cellWidth: 100 }
          }
        },
        bodyStyles: { fontSize: 7, lineWidth: 1, cellPadding: 5 }
      });
      doc.save(`${this.commonService.Date()}_Finance.pdf`);
    }
    //-------------------FinanceProject Finance Excel------------------------------
    ExcelFinances(title, header, data, footer) {
      let workbook = new Excel.Workbook();
  
      let worksheet = workbook.addWorksheet("ProjectDetails");
  
      let titleRow = worksheet.addRow([title]);
  
      titleRow.getCell(1).fill = {
        type: "pattern",
        pattern: "solid",
        fgColor: { argb: "0c0b38" }
      };
      // Set font, size and style in title row.
      worksheet.properties.defaultRowHeight = 20;
      worksheet.properties.defaultRowWidth = 500;
      titleRow.font = {
        name: "Calibri",
        family: 4,
        size: 14,
        underline: "double",
        bold: true
      };
  
      // Blank Row
  
      worksheet.addRow([]);
  
      worksheet.mergeCells("A1:F1");
  
      worksheet.addRow([]);
  
      worksheet.columns = [
        { width: 5 },
  
        { width: 15 },
  
        { width: 10 },
  
        { width: 10 },
  
        { width: 10 },
  
        { width: 10 }
      ];
  
      let headerRow = worksheet.addRow(header);
  
      headerRow.font = { bold: true };
  
      headerRow.eachCell((cell, number) => {
        cell.fill = {
          type: "pattern",
  
          pattern: "solid",
  
          fgColor: { argb: "0c0b38" },
  
          bgColor: { argb: "FFFFFF00" }
        };
  
        cell.border = {
          top: { style: "thin" },
  
          left: { style: "thin" },
  
          bottom: { style: "thin" },
  
          right: { style: "thin" }
        };
      });
  
      worksheet.addRows(data);
  
      worksheet.addRows([]);
  
      worksheet.addRows([]);
  
      //Footer Row
      worksheet.getRow(1).alignment = {
        horizontal: "center",
        vertical: "middle"
      };
      let footerRow = worksheet.addRow(footer);
  
      footerRow.font = { bold: true };
  
      footerRow.getCell(3).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      footerRow.getCell(4).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      footerRow.getCell(5).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      footerRow.getCell(6).fill = {
        type: "pattern",
  
        pattern: "solid",
  
        fgColor: { argb: "0c0b38" }
      };
  
      workbook.xlsx.writeBuffer().then(data => {
        let blob = new Blob([data], {
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        });
  
        fs.saveAs(blob, `${this.commonService.Date()}_FinancialReport.xlsx`);
      });
    }



    //-------------------Delayed Project REoprt------------------------------

    DelayedReportes(type) {
    let Sanctioned = 0;
    let NoOfProjects = 0;

    const title = "Projects Details Report";

    const header = [
      "#",

      "Bucket",

      "Total Projects",

      "Sanctioned Cost (In Lakhs)"
    ];

    let data = [];

    this.Delaye.forEach((element, key) => {
      let newArray = [];

      newArray.push(
        key + 1,

        element.Bucket ? element.Bucket : "-",

        element.TOTALPROJECT ? parseFloat(element.TOTALPROJECT) : 0,

        element.BUDGETALLOCATED
          ? parseFloat(this.convert(element.BUDGETALLOCATED))
          : 0
      );

      data[key] = newArray;

      Sanctioned += element.BUDGETALLOCATED
        ? parseFloat(this.convert(element.BUDGETALLOCATED))
        : 0;

      NoOfProjects += element.TOTALPROJECT
        ? parseFloat(element.TOTALPROJECT)
        : 0;
    });

    const footer = ["", "", "", parseFloat(NoOfProjects.toFixed(2)),parseFloat(Sanctioned.toFixed(2))];

    if (type == "Excel") {
      console.log("title", title);
      this.ExcelDelayed(title, header, data, footer);
    }
    if (type == "PDF") {
      this.PdfDelayed(title, header, data, footer);
    }
  }
  //----------------Delayed Excel----------------------
  ExcelDelayed(title, header, data, footer) {
    let workbook = new Excel.Workbook();

    let worksheet = workbook.addWorksheet("ProjectDetails");

    let titleRow = worksheet.addRow([title]);

    titleRow.getCell(1).fill = {
      type: "pattern",
      pattern: "solid",
      fgColor: { argb: "0c0b38" }
    };
    // Set font, size and style in title row.
    worksheet.properties.defaultRowHeight = 20;
    worksheet.properties.defaultRowWidth = 500;
    titleRow.font = {
      name: "Calibri",
      family: 4,
      size: 14,
      underline: "double",
      bold: true
    };

    // Blank Row

    worksheet.addRow([]);

    worksheet.mergeCells("A1:E1");

    worksheet.addRow([]);

    worksheet.columns = [
      { width: 5 },

      { width: 20 },

      { width: 10 },

      { width: 10 }
    ];

    let headerRow = worksheet.addRow(header);

    headerRow.font = { bold: true };

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",

        pattern: "solid",

        fgColor: { argb: "0c0b38" },

        bgColor: { argb: "FFFFFF00" }
      };

      cell.border = {
        top: { style: "thin" },

        left: { style: "thin" },

        bottom: { style: "thin" },

        right: { style: "thin" }
      };
    });

    worksheet.addRows(data);

    worksheet.addRows([]);

    worksheet.addRows([]);

    //Footer Row
    worksheet.getRow(1).alignment = {
      horizontal: "center",
      vertical: "middle"
    };
    let footerRow = worksheet.addRow(footer);

    footerRow.font = { bold: true };

    footerRow.getCell(3).fill = {
      type: "pattern",

      pattern: "solid",

      fgColor: { argb: "0c0b38" }
    };

    footerRow.getCell(5).fill = {
      type: "pattern",

      pattern: "solid",

      fgColor: { argb: "0c0b38" }
    };

    workbook.xlsx.writeBuffer().then(data => {
      let blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });

      fs.saveAs(blob, `${this.commonService.Date()}_DelayedProjectReport.xlsx`);
    });
  }
  //----------delayed PDF------------
  PdfDelayed(title, header, data, footer) {
    const doc = new jsPDF("l", "pt", "tabloid");
    doc.setFontType("underline");
    doc.text(512, 60, title);

    doc.setProperties({
      title: `Project Details Export`
    });
    data[data.length] = footer;
    doc.autoTable({
      head: [header],
      margin: { top: 80 },
      headStyles: { europe: { halign: "center" } },
      body: data,
      columnStyles: {
        columnStyles: {
          0: { cellWidth: 40 },
          1: { cellWidth: 100 },
          2: { cellWidth: 100 },
          3: { cellWidth: 100 },
          
        }
      },
      bodyStyles: { fontSize: 7, lineWidth: 1, cellPadding: 5 }
    });
    doc.save(`${this.commonService.Date()}_Delayed.pdf`);
  }

//projectStatus

ProjectStatusReportes(type) {
    let Sanctioned = 0;
    let NoOfProjects = 0;

    const title = "Projects Details Report";

    const header = [
      "#",

      "Display Status",

      "Total Project",

      "Sactioned Cost (In Lakhs)"
    ];

    let data = [];

    this.ProjectStatuss.forEach((element, key) => {
      let newArray = [];

      newArray.push(
        key + 1,

        element.DisplayStatus ? element.DisplayStatus : "-",

        element.TOTALPROJECT ? parseFloat(element.TOTALPROJECT) : 0,

        element.BUDGETALLOCATED
          ? parseFloat(this.convert(element.BUDGETALLOCATED))
          : 0
      );

      data[key] = newArray;

      Sanctioned += element.BUDGETALLOCATED
        ? parseFloat(this.convert(element.BUDGETALLOCATED))
        : 0;

      NoOfProjects += element.TOTALPROJECT
        ? parseFloat(element.TOTALPROJECT)
        : 0;
    });

    const footer = ["", "", NoOfProjects, Sanctioned.toFixed(2)];


    if (type == "Excel") {
      console.log("title", title);
      this.ExportInExcelProjectStatusReports(title, header, data, footer);
    }
    if (type == "PDF") {
      this.ExportInPdfProjectStatusReports(title, header, data, footer);
    }


  }
  ExportInExcelProjectStatusReports(title, header, data, footer) {
    let workbook = new Excel.Workbook();

    let worksheet = workbook.addWorksheet("ProjectDetails");

    let titleRow = worksheet.addRow([title]);

    titleRow.getCell(1).fill = {
      type: "pattern",
      pattern: "solid",
      fgColor: { argb: "0c0b38" }
    };
    // Set font, size and style in title row.
    worksheet.properties.defaultRowHeight = 20;
    worksheet.properties.defaultRowWidth = 500;
    titleRow.font = {
      name: "Calibri",
      family: 4,
      size: 14,
      underline: "double",
      bold: true
    };

    // Blank Row

    worksheet.addRow([]);

    worksheet.mergeCells("A1:D1");

    worksheet.addRow([]);

    worksheet.columns = [
      { width: 5 },

      { width: 15 },

      { width: 10 },

      { width: 10 }
    ];

    let headerRow = worksheet.addRow(header);

    headerRow.font = { bold: true };

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",

        pattern: "solid",

        fgColor: { argb: "0c0b38" },

        bgColor: { argb: "FFFFFF00" }
      };

      cell.border = {
        top: { style: "thin" },

        left: { style: "thin" },

        bottom: { style: "thin" },

        right: { style: "thin" }
      };
    });

    worksheet.addRows(data);

    worksheet.addRows([]);

    worksheet.addRows([]);

    //Footer Row
    worksheet.getRow(1).alignment = {
      horizontal: "center",
      vertical: "middle"
    };
    let footerRow = worksheet.addRow(footer);

    footerRow.font = { bold: true };

    footerRow.getCell(3).fill = {
      type: "pattern",

      pattern: "solid",

      fgColor: { argb: "0c0b38" }
    };

    footerRow.getCell(4).fill = {
      type: "pattern",

      pattern: "solid",

      fgColor: { argb: "0c0b38" }
    };

    workbook.xlsx.writeBuffer().then(data => {
      let blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });

      fs.saveAs(blob, `${this.commonService.Date()}_ProgressStatusReport.xlsx`);
    });
  }

  ExportInPdfProjectStatusReports(title, header, data, footer){
    const doc = new jsPDF("l", "pt", "tabloid");
    doc.setFontType("underline");
    doc.text(512, 60, title);

    doc.setProperties({
      title: `Project Details Export`
    });
    data[data.length] = footer;
    doc.autoTable({
      head: [header],
      margin: { top: 80 },
      headStyles: { europe: { halign: "center" } },
      body: data,
      columnStyles: {
        columnStyles: {
          0: { cellWidth: 40 },
          1: { cellWidth: 100 },
          2: { cellWidth: 100 },
          3: { cellWidth: 100 }
 
        }
      },
      bodyStyles: { fontSize: 7, lineWidth: 1, cellPadding: 5 }
    });
    doc.save(`${this.commonService.Date()}_ProjectStatus.pdf`);

  }



  // progressReports

  progressReports(type) {
    let Sanctioned = 0;
    let NoOfProjects = 0;

    const title = "Projects Details Report";

    const header = [
      "#",

      "Display Status",

      "Total Project",

      "Sactioned Cost (In Lakhs)"
    ];

    let data = [];

    this.ProgressData.forEach((element, key) => {
      let newArray = [];

      newArray.push(
        key + 1,

        element.DisplayStatus ? element.DisplayStatus : "-",

        element.TOTALPROJECT ? parseFloat(element.TOTALPROJECT) : 0,

        element.BUDGETALLOCATED
          ? parseFloat(this.convert(element.BUDGETALLOCATED))
          : 0
      );

      data[key] = newArray;

      Sanctioned += element.BUDGETALLOCATED
        ? parseFloat(this.convert(element.BUDGETALLOCATED))
        : 0;

      NoOfProjects += element.TOTALPROJECT
        ? parseFloat(element.TOTALPROJECT)
        : 0;
    });

    const footer = ["", "", NoOfProjects, Sanctioned.toFixed(2)];


    if (type == "Excel") {
      console.log("title", title);
      this.ExportInExcelprogressReports(title, header, data, footer);
    }
    if (type == "PDF") {
      this.ExportInPdfprogressReports(title, header, data, footer);
    }


  }
  ExportInExcelprogressReports(title, header, data, footer) {
    let workbook = new Excel.Workbook();

    let worksheet = workbook.addWorksheet("ProjectDetails");

    let titleRow = worksheet.addRow([title]);

    titleRow.getCell(1).fill = {
      type: "pattern",
      pattern: "solid",
      fgColor: { argb: "0c0b38" }
    };
    // Set font, size and style in title row.
    worksheet.properties.defaultRowHeight = 20;
    worksheet.properties.defaultRowWidth = 500;
    titleRow.font = {
      name: "Calibri",
      family: 4,
      size: 14,
      underline: "double",
      bold: true
    };

    // Blank Row

    worksheet.addRow([]);

    worksheet.mergeCells("A1:D1");

    worksheet.addRow([]);

    worksheet.columns = [
      { width: 5 },

      { width: 15 },

      { width: 10 },

      { width: 10 }
    ];

    let headerRow = worksheet.addRow(header);

    headerRow.font = { bold: true };

    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: "pattern",

        pattern: "solid",

        fgColor: { argb: "0c0b38" },

        bgColor: { argb: "FFFFFF00" }
      };

      cell.border = {
        top: { style: "thin" },

        left: { style: "thin" },

        bottom: { style: "thin" },

        right: { style: "thin" }
      };
    });

    worksheet.addRows(data);

    worksheet.addRows([]);

    worksheet.addRows([]);

    //Footer Row
    worksheet.getRow(1).alignment = {
      horizontal: "center",
      vertical: "middle"
    };
    let footerRow = worksheet.addRow(footer);

    footerRow.font = { bold: true };

    footerRow.getCell(3).fill = {
      type: "pattern",

      pattern: "solid",

      fgColor: { argb: "0c0b38" }
    };

    footerRow.getCell(4).fill = {
      type: "pattern",

      pattern: "solid",

      fgColor: { argb: "0c0b38" }
    };

    workbook.xlsx.writeBuffer().then(data => {
      let blob = new Blob([data], {
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      });

      fs.saveAs(blob, `${this.commonService.Date()}_ProgressStatusReport.xlsx`);
    });
  }

  ExportInPdfprogressReports(title, header, data, footer){
    const doc = new jsPDF("l", "pt", "tabloid");
    doc.setFontType("underline");
    doc.text(512, 60, title);

    doc.setProperties({
      title: `Project Details Export`
    });
    data[data.length] = footer;
    doc.autoTable({
      head: [header],
      margin: { top: 80 },
      headStyles: { europe: { halign: "center" } },
      body: data,
      columnStyles: {
        columnStyles: {
          0: { cellWidth: 40 },
          1: { cellWidth: 100 },
          2: { cellWidth: 100 },
          3: { cellWidth: 100 }
 
        }
      },
      bodyStyles: { fontSize: 7, lineWidth: 1, cellPadding: 5 }
    });
    doc.save(`${this.commonService.Date()}_ProjectStatus.pdf`);

  }

}
