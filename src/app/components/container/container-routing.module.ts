import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { AuthGuardService } from "../../guards/auth-guard.service";

import { TracesComponent } from "./tracespage/traces.component";
import { RegistrationsComponent } from "./registrations/registrations.component";
import { InfectedComponent } from "./infected-peoples/infected.component";
import { ViewreportComponent } from './viewreport/viewreport.component';
import { ChartviewComponent } from './viewreport/chartview/chartview.component';
import { TreeviewComponent } from './viewreport/treeview/treeview.component';

const routes: Routes = [
  { path: "", component: DashboardComponent,data: { title: "Dashboard" }, pathMatch: "full"  },
  
  

  { path: "traces", component: TracesComponent, data: { title: "Traces" } },
  
  { path: "registrations", component: RegistrationsComponent, data: { title: "Registrations" } },
  
  { path: "infectedPeople", component: InfectedComponent, data: { title: "Infected People" } },
  
  { path: "tracereport/list", component: ViewreportComponent, data: { title: "Trace Report" } },
  { path: "tracereport/list/:MobileNo", component: ViewreportComponent, data: { title: "Trace Report" } },
  
  { path: "tracereport/chart", component: ChartviewComponent, data: { title: "Trace Report" } }, 
  { path: "tracereport/chart/:MobileNo", component: ChartviewComponent, data: { title: "Trace Report" } }, 
  
  { path: "tracereport/tree", component: TreeviewComponent, data: { title: "Trace Report" } }, 
  { path: "tracereport/tree/:MobileNo", component: TreeviewComponent, data: { title: "Trace Report" } }, 


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContainerRoutingModule { }
