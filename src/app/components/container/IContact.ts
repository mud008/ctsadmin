export interface IContact {
    MobileNo: string;
    Level: string;
    img: string;
    Contacts: IContact[];
}
export declare class Contact implements IContact {
    MobileNo: string;
    Level: string;
    img: string;
    Contacts: IContact[];
    constructor(orgStructure: string[], manager?: Contact);
}
