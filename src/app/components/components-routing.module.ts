import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./container/dashboard/dashboard.component";
import { AuthGuardService } from "../guards/auth-guard.service";
const routes: Routes = [
  { path: "", component: DashboardComponent, data: {title: "Dashboard"},pathMatch: "full" },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
