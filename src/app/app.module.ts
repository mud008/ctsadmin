import { BrowserModule } from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { AgmCoreModule } from "@agm/core";
import {HttpClientModule} from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
import { ChartsModule } from "ng2-charts";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgxUiLoaderModule } from "ngx-ui-loader";
import { OrgChartModule } from 'ng2-org-chart';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

//Layout
import { ComponentsComponent } from "../app/components/components.component";
import { HeaderComponent } from "../app/components/common/header/header.component";
import { UiSettingsComponent } from "../app/components/common/ui-settings/ui-settings.component";
import { MenubarComponent } from "../app/components/common/menubar/menubar.component";
import { WrapperFooterComponent } from "../app/components/common/wrapper-footer/wrapper-footer.component";

//providers
import {DatePipe} from "@angular/common";

//Component
import { LoginComponent } from "../app/components/login/login.component";
import { ToastrModule } from "ngx-toastr";

@NgModule({
  declarations: [
    AppComponent,
    ComponentsComponent,
    HeaderComponent,
    UiSettingsComponent,
    MenubarComponent,
    WrapperFooterComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    OrgChartModule,
    SweetAlert2Module,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyClXoXjRap5nFM-6A7Gj-loN7pR_dOHyck",
      libraries: ["places"] 
    }),
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    ChartsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxUiLoaderModule,
  ],
  providers: [
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
