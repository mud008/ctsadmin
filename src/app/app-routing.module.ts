import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "../app/components/login/login.component";
import { ComponentsComponent } from "../app/components/components.component";

const routes: Routes = [
    { 
      path: "",
      component: ComponentsComponent,
      children: [
        { path: "", loadChildren: () => import("./components/container/container.module").then(m => m.ContainerModule) }
      ]
    },

    //no layout routes
    { path: "login", component: LoginComponent, data: {title: "Login"} },

    // otherwise redirect to home
    { path: "**", redirectTo: "login" }

  ];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
